# Integration with Express

---

## Objectives
1. Use Chai HTTP to create tests that would check http connections
2. Run specific test cases or test suites

---

## Setup

1. Create an object in util.js to simulate a set of names and ages
```javascript
const names = {
	"Brandon": {
		"name" : "Brandon Boyd",
		"age" : 35
	}, 
	"Steve" : {
		"name" : "Steve Tyler",
		"age" : 56
	}
}


// function factorial(n){
// //	if(typeof n !== 'number') return undefined; //remove this first
// //	if(n<0) return undefined; //remove this first
// 	if(n===0) return 1;
// 	if(n===1) return 1;
// 	return n * factorial(n-1);
// }

module.exports = {
	factorial: factorial,
	names: names
}
```
2. Create an app folder in the project's root directory and in it, a file called routes.js 
```javascript
const { names } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/people', (req, res) => {
		return res.send({
			people: names
		});
	})

	app.post('/person', (req, res) => {
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - NAME has to be a string'
			})
		}
		if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter AGE'
			})
		}
		if(typeof req.body.age !== "number"){
			return res.status(400).send({
				'error': 'Bad Request - AGE has to be a number'
			})	
		}
	})
}
```
3. Install Express to create a server
```
npm install express
```
4. Populate the server.js to include the route.
```javascript
const express = require('express');

const app = express();

const PORT = 5001;

app.use(express.json());

require('./app/routes')(app, {});

app.listen(PORT, () => {
	console.log('Running on port ' + PORT);
})
```
5. Create a new test file called routes_test.js in the test folder
```javascript
const chai = require('chai');
const expect = chai.expect;

//same as const { expect } = require("chai")

const http = require('chai-http');
chai.use(http);

describe("api_test_suite", () => {
	it("test_api_get_people_is_running", () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_get_people_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('test_api_post_person_returns_400_if_no_person_name', (done) => {		
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
		    alias: "Jason",
	      	age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

})
```
5. Open a new terminal to run the server
```
npm start
```
6. Open a another terminal to run the test WHILE the server is running.
```
npm run test
```
---
## Elaboration

Chai-http is a module that allows testing for API endpoint consumption.
.request() is a chai method that accepts the server url to be used
.get() specifies the type of http request as GET and accepts the API endpoint
.post() specifies the type of http request as POST
.type() specifies the type of input to be sent out as part of the POST request
.send() specifies the data to be sent as part of the POST request
.end() is the method that handles the response and error that will be received from the endpoint
.done() states that the test is currently done

---
## Specifying test suites

Test suites can be specified by installing mocha globally
```
npm install -g mocha
```

This allows the system to see mocha and run tests independently.

Create a new script in package.json:

```javascript
  "scripts": {
    "dev": "nodemon server.js",
    "test": "mocha --recursive",
    "specifictest": "mocha -g"
  }
```

To run a specific test or test suite, do the following
```javascript
npm run-script specifictest "api_test_suite"
```

---
# Activity

## Instructions that can be provided to the students for reference:

1. In your route_test.js, Add following test cases in the api_test_suite:
	-	Check if the post endpoint is running
	- Check if the post endpoint encounters an error if there is no alias
	- Check if the post endpoint encounters an error if there is no age
2. Add your updates to your unit_test online repo and push to git with the commit message of “Add activity code S3".
3. Add the link in Boodle.

## Solution

routes.js
```js
module.exports = (app) => {

	//...

	app.post('/person', (req, res) => {
		
		//...

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request: Alias is missing property'
			})
		}

	})
}
```
routes_test.js
```js
describe("api_test_suite", () => {

	//...

	it("test_api_post_person_is_running", () => {
		chai.request('http://localhost:5001')
		.post('/people')
		.type('json')
		.send({
			alias: "Jay",
			name: "Jay White",
			age: 27
		})
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_post_person_returns_400_if_no_ALIAS', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "Jay White",
			age: 27
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Jay",
			name: "Jay White",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})


})
```