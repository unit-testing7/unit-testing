# Test Driven Development

--- 

## Objectives
1. Understand what Test Driven Development is
2. Understand the process of creating functionalities using the TDD mindset
3. Create tests and refactor functions using the TDD process

---

## Supplemental Materials

---
## Test Driven Development

Test Driven Development
- A mindset wherein tests drive development and design
- Tests are written before the creation/enhancement of the code

TDD is not just tests per se - it is about the underlying design and development processes.
*Consider how components mesh together.*
*Consider how efficiently one part of your codes work with the other codes.*
*Ex: the habit of adding console logs and forgetting them*

---
## TDD Process

The TDD Process follows a repetition of 3 steps in a cycle
1. Write a test that fails
2. Write code to pass the test
3. Refactor the newly-written code as needed

![TDD Diagram](TDD.png)

---
## Using the TDD Process

Recall the factorial functionality in the util.js
```javascript
function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}
```

What if there is a user who enters a negative number? So a test needs to be written for that.
In test.js, the test can be written and check the output of the test

```javascript
describe('test_fun_factorials', ()=>{
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})	

	//Test for negative numbers
	it("test_fun_factorials_neg1_is_undefined", () => {
		const product = -1;
		expect(product).to.equal(undefined);
	})
})
```

The test will fail as -1 is not undefined. Note that factorial(-1) is not used as it will incur a infinite loop.
```
npm run-script specifictest "test_fun_factorials" 
```
![Failed Test](Capture.JPG)

The test then can be modified to include the factorial function
```javascript
	it("test_fun_factorials_neg1_is_undefined", () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	})
```

Create now the functionality to check the scenario for -1
```javascript
function factorial(n){
	//Add this line
	if(n < 0) return undefined;
	//End of line
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}
```

Run the test again
![Passed Test](Capture2.JPG)

Consider other scenarios and write the corresponding tests and write the code to pass the test
Ex. What if the factorial function was to take in nonnumeric values?

Mini-Activity

- Create a failing test to test a scenario if a user enters data that is not a number.
	- Assert/Expect the product to be undefined.
- Refactor the factorial method to accomodate the scenario of a numerical string.

Solution:
![Mini-Activity Solution](Screenshot_1.png)

---
# Activity

### Note: Provide Updated routes.js and routes_test.js to students via boodle notes.

## Instructions that can be provided to the students for reference:

1. In your routes, Add a new route provided by your instructor.
2. In your routes_test, Add the new test cases for your api_test_suite provided by your instructor.
3. Check and run the api_test_suite.
4. Identify the failing tests.
5. Refactor and update the route to pass the tests.
3. Add your updates to your unit_test online repo and push to git with the commit message of “Add activity code S4".
4. Add the link in Boodle.

**Stretch Goal**

1. Create a new test case in the api_test_suite to check a scenario where the user sends a request with a wrong password.
	- Expect the response status code to be 403.
2. Refactor the '/login' route and add a response to this scenario.

## Solution

routes.js
```js
	app.post('/login',(req,res)=>{

		let foundUser = users.find((user) => {

			return user.username === req.body.username && user.password === req.body.password

		});

		if(!req.body.hasOwnProperty(username)){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter USERNAME'
			})
		}

		if(!req.body.hasOwnProperty(password)){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter PASSWORD'
			})
		}


		if(foundUser){
			return res.status(200).send({
				'success': 'Thank you for logging in.'
			})
		}

	})
```

### Stretch Goal

routes_test.js
```js
describe("api_test_suite", () => {
	
	//...

	it('stretch_goal_post_login_returns_403_if_wrong_credentials', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "87brandon1999",
		})
		.end((err, res) => {
			expect(res.status).to.equal(403)
			done();
		})
	})

})
```
routes.js
```js
module.exports = (app) => {

	//...

	app.post('/login',(req,res)=>{

		//...

		if(!foundUser){
			return res.status(403).send({
				'forbidden': 'Wrong Credentials'
			})
		}

	})
}
```