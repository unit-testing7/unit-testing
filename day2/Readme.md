# Writing Tests

---

## Objectives
1. Create simple tests using mocha and chai
2. Compare and contrast assert and expect assertions
3. Create test suites
4. Run tests using npm

---

## Setup

As from the previous discussion, the following modules will be needed

* chai - the assertion library
* chai-http - for handling requests
* mocha - the unit testing framework

1. Create a new folder in the WDC035 folder called unit_test.
2. Inside the unit_test folder, open a new terminal and input the following command:
```
npm init
npm install chai chai-http mocha
```
*Optional: nodemon can be installed to ease up restarting the server*

4. Modify the package.json file to include the following:
```javascript
"scripts" : {
	"dev" : "nodemon server.js",
	"test" : "mocha --recursive"
}
```

5. Create test and src folders in the root folder of the project and server.js file in the root, and util.js in the src folder

6. Populate util.js with a factorial function
```javascript
function factorial(n){
	if(typeof n !== 'number') return undefined; //remove this first
	if(n<0) return undefined; //remove this first
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

module.exports = {
	factorial: factorial
}
```
7. Create a test.js file in the test folder.

---

## Writing tests

Recall the rules of unit testing
1. Don’t write production code unless it is to make a failing test pass.
2. Don’t write a unit test that tests for multiple conditions.
3. Don’t write any more of production code that is sufficient to pass one failing unit test.

The test can be run using npm test

1. Populate the test.js by including a test to check the factorial
```javascript
const { factorial } = require('../src/util.js');

const { expect, assert } = require('chai');

it('test_fun_factorial_5!_is_120', () => {
	const product = factorial(5);
	expect(product).to.equal(120);
})

it('test_fun_factorial_1!_is_1', () => {
	const product = factorial(1);
	assert.equal(product, 1);
})
```

2. Run the test and check if the test passes

---

## Breakdown of code

```javascript
const { factorial } = require('../src/util.js');

const { expect, assert } = require('chai'); //required for the assertion to be tested

it('test_fun_factorial_5!_is_120', () => {
	const product = factorial(5);
	expect(product).to.equal(120);
})

it('test_fun_factorial_1!_is_1', () => {
	const product = factorial(1);
	assert.equal(product, 1);
})
```

const { expect, assert } = require('chai'); -> This line gets the expect and assert functions from chai to be used.

it() accepts two parameters - a string explaining what the test should do, and a callback function which contains the actual test

expect and assert are assertions or conditions for the test to pass. If the assertion fails, then the test is considered as failed.

### Test Suites

Test Suites are made up of collection of test cases that should be executed together. 

The describe keyword is used to group tests together

```javascript
describe('test_fun_factorials', ()=>{
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})	
})
```

---

## Assert vs Expect

Both Assert and Expect can be used in testing but there are slight differences

The assert uses the assert-dot notation that node.js has. The assert module also provides several additional tests.

[Reference](https://www.chaijs.com/api/assert/)
Ex.
```javascript
assert.equal(x, 2, "x is equal to 2")
```
The expect module uses chainable language to construct assertions

[Reference](https://www.chaijs.com/api/bdd/)
Ex.
```javascript
expect(x).to.equal(2);
```

---

# Activity

## Instructions that can be provided to the students for reference:

1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.

2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 and 7.
	- If the number received is divisible by 5, return true.
	- If the number received is divisible by 7, return true.
	- Return false if otherwise

3. Create 4 test cases in a test suite in test.js that would check if the functionality of div_check is correct.

4. Create a git repository named unit_test.
5. "Initialize your local git repository, add the remote link and push to git with the commit message of Add activity code S2".
6. Add the link in Boodle.


## Solution

Create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.

```js
describe('test_fun_factorials', ()=>{
	
	...

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0)
		assert.equal(product, 1)
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4)
		expect(product).to.equal(24)
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10)
		expect(product).to.equal(3628800)


})
```

Create a function called div_check in util.js that checks a number if it is divisible by 5 and 7.
- If the number received is divisible by 5, return true.
- If the number received is divisible by 7, return true.
- Return false if otherwise

```js
function div_check(n){

	if(n%5 === 0) return true
	if(n%7 === 0) return true
	return false

}
```

Create 4 test cases in a test suite in test.js that would check if the functionality of div_check is correct.

```js
describe('test_divisibilty_by_5_or_7', ()=>{

	it('test_100_is_divisible_by_5', () => {
		const number = div_check(100);
		expect(number).to.equal(true);
	})

	it('test_49_is_divisible_by_7', () => {
		const number = div_check(49);
		expect(number).to.equal(true);
	})	


	it('test_30_is_divisible_by_5', () => {
		const number = div_check(30);
		expect(number).to.equal(true);
	})

	it('test_56_is_divisible_by_7', () => {
		const number = div_check(56);
		expect(number).to.equal(true);
	})	

})
```
