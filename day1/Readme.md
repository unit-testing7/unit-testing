# Intro to Unit Testing



---

## Objectives
At the end of the session, the students are expected to:
- Define unit testing
- Describe “unit” in the context of unit testing
- Differentiate the rules for unit testing
- Setup the dependencies for Mocha Chai

---

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/short-courses/unit_testing)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1FRTes72tEBYx0LMvn079A5FMzpOhdhGuDFViw_G85HE/edit#slide=id.g11449381975_0_1263)

---

## Supplemental Materials

- [What is Testing?](https://www.guru99.com/software-testing-introduction-importance.html#1)
- [What is Unit Testing?](https://smartbear.com/learn/automated-testing/what-is-unit-testing/)
- [What is Chai?](https://www.chaijs.com/)
- [What is Chai-Http?](https://www.chaijs.com/plugins/chai-http/#:~:text=Chai%20HTTP%20provides%20an%20interface,that%20you%20wish%20to%20invoke.)
- [What is Mocha?](https://mochajs.org/#:~:text=Mocha%20is%20a%20feature%2Drich,Hosted%20on%20GitHub.)

---

# Lesson Proper

## What testing is for

Testing is the process of executing a program or application with the intent of finding errors or other defects, and verifying that the product is fit for use. 

Testing can be as simple as the following scenario:

*You write your code on your editor and then switch to the browser or/and database to check and test if your code is working.
If your code did not work, you go back to your text editor and check what went. otherwise, you proceed in writing your new lines of code.*

---

## Unit Testing

Unit testing is a principle wherein code is tested as small, isolated units.

A unit then is an atomic value of any software or system. There is no single definition of what a unit can be in a system and it boils down to what is agreed upon as the smallest part to be tested.

A unit can be the following:
* a single function
* each endpoint in a backend API
* a workflow in the BE or FE
* responsiveness of user experience

In web development, unit tests can be categorically divided into front-end and backend tests.

Unit tests on the front-end usually test for completion of HTML components and visible styles. They are designed that given a certain page, all materials are in place, including validation mechanisms.

On the other hand, back-end testing may want to test functions ran on the server side, or API consumption.


---

## Importance of unit testing

Consider the following scenario:

*Consider a team of five (5) that will work on different but integrated modules on an API over the span of 12 months.*

*Assume that initially, the API returned data structures as a bare JSON, such as:*
```javascript
{
	"_id" : ObjectId("5dc210129e12f019c"),
	"name" : "Brandon Brandon",
	"age" : 18
}
```

*Assume that there is a unit test ready for a “getOneUser” function, and the test is ran.*

*However, what if someone in the team decided that it would make more sense to encapsulate response data within a “data” object. The unit test will be able to identify that the return value was suddenly different, without having to manually check it.*

```javascript
{
	"data":	{
		"_id" : ObjectId("5dc210129e12f019c"),
		"name" : "Brandon Brandon",
		"age" : 18
	}
}
```

Bundled together, unit tests can readily identify lapses or issues with code in a sustainable manner.

Unit tests are usually integrated with CI / CD tools, to identify code-breaking changes during compile/release.

Note:
Continuous integration (CI) and continuous delivery (CD), also known as CI/CD, embodies a culture, operating principles, and a set of practices that application development teams use to deliver code changes more frequently and reliably.

---
## Challenges in Unit Testing

1. Nearly infinite tests that can be written therefore it requires a lot of effort. Exhaustive.
![Flowchart](Capture.JPG)
2. Simply there is not enough developers who have the mindset

Since it’s difficult by nature, the value of developers who can do this is higher.

---
## Rules of Unit Testing
1. Don’t write production code unless it is to make a failing unit test pass. You might be deploying code that doesn't work.
2. Don’t write a unit test for multiple conditions or write any more of a unit test than what is sufficient to fail. This is to prevent scope creep. Trying to do too many things at once may cause you to lose direction.
3. Don’t write more production code than what is sufficient to pass one failing unit test. To prevent scope-creep, do not add more features if the test has already passed.


Note:
Scope creep is what happens when changes are made to the project scope without any control procedure like change requests. Those changes also affect the project schedule, budget, costs, resource allocation and might compromise the completion of milestones and goals.

---
## Unit Testing in Javascript

In order for testing to be done in JS, the following node modules will be used:

* chai - the assertion library
* chai-http - for handling requests
* mocha - the unit testing framework


Notes:
Chai is an assertion library that is mostly used alongside Mocha.
It has several interfaces that a developer can choose from and looks much like writing tests in English sentences.

Chai HTTP provides an interface for live integration testing. Used to test a request via url.

Mocha is a feature-rich JavaScript test framework running on Node. js and in the browser.


```
npm init
npm install chai chai-http mocha
```

In package.json, change the test script to the following:
```javascript
"scripts":{
	"test": "mocha --recursive"
}
```

With that, tests can now be created and run.

---

## Exercise

Setup the project for the unit testing module

1. Create a git repository named S1.
2. "Initialize your local S1 as a git repository, add the remote link and push to git with the commit message of Add activity code S1".
3. Add the link in Boodle.